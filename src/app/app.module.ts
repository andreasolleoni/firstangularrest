import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { SearchByIdService } from './search-by-id.service';
import { InsertOrModifyService } from './insert-or-modify.service';
import { SearchComponent } from './search/search.component';
import { InsertComponent } from './insert/insert.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableButtonsComponent } from './table-buttons/table-buttons.component';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { ModalsComponent } from './modals/modals.component'
import { RouterModule, Routes, Router } from '@angular/router';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

export const routes: Routes = [
  { path: "",
  redirectTo: '/home',
  pathMatch: 'full'
  },
  { path: "home", component: SmartTableComponent },
  { path: "home/:timestampVal", component: SmartTableComponent },
  { path: "**", component: SmartTableComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    InsertComponent,
    UpdateComponent,
    DeleteComponent,
    NavbarComponent,
    SmartTableComponent,
    TableButtonsComponent,
    ModalsComponent,
  ],
  entryComponents: [
    TableButtonsComponent,
    ModalsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2SmartTableModule,
    Angular2FontawesomeModule,
    RouterModule.forRoot(routes),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  providers: [
    SearchByIdService,
    InsertOrModifyService, 
    ModalsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
