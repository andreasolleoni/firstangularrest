import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Product } from '../product';
import { InsertOrModifyService } from '../insert-or-modify.service';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  
  showHide;
  constructor(private insOrMod: InsertOrModifyService) { 
    this.showHide = true;
  }

  ngOnInit() {
  }

}
