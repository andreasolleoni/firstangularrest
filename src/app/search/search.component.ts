import { Component,OnInit,NgModule  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Product } from '../product';
import { SearchByIdService } from '../search-by-id.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  
  showHide;
  constructor(private searchById: SearchByIdService) {
    this.showHide = true;
    }
  ngOnInit(){}
}
