import { Component, OnInit, Input, Output } from '@angular/core';
import { SearchByIdService } from '../search-by-id.service';
import { InsertOrModifyService } from '../insert-or-modify.service';
import { ModalsComponent } from '../modals/modals.component';

@Component({
  selector: 'app-table-buttons',
  templateUrl: './table-buttons.component.html',
  styleUrls: ['./table-buttons.component.css']
})
export class TableButtonsComponent implements OnInit {
  @Input() rowData;

  constructor(private modals: ModalsComponent) { 
  }

  displayDeleteRow(){
    this.modals.confirmDelete(this.rowData);
  }

  displayEditRow(){
    this.modals.confirmEdit(this.rowData);
  }

  ngOnInit() { 
  }

}
