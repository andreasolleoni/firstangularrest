import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Product } from '../product';
import { SearchByIdService } from '../search-by-id.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  showHide;
  constructor(private searchById: SearchByIdService) { 
    this.showHide = true;
  }
  ngOnInit() {}

}
