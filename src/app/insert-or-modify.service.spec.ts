import { TestBed, inject } from '@angular/core/testing';

import { InsertOrModifyService } from './insert-or-modify.service';

describe('InsertOrModifyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InsertOrModifyService]
    });
  });

  it('should be created', inject([InsertOrModifyService], (service: InsertOrModifyService) => {
    expect(service).toBeTruthy();
  }));
});
