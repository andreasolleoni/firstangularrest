import { Injectable, NgZone } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
import { Http, Response } from '@angular/http';
import { Observable, Timestamp } from 'rxjs';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { RouterModule, Routes, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import 'rxjs/add/operator/map';

@Injectable()
export class InsertOrModifyService {
  
  constructor(private http: Http, private router: Router, private ngZone: NgZone, private spinnerService: Ng4LoadingSpinnerService){
  }

  insert(idProdInsert, nameProdInsert, categoryProdInsert, priceProdInsert){
    var product = {
      "id": parseInt(idProdInsert),
      "name": nameProdInsert.trim(),
      "category": categoryProdInsert.trim(),
      "unitPrice": parseFloat(priceProdInsert) 
    };
    $('#insertProdModal #idProdInsertInput').removeClass('error');
    $('#insertProdModal #nameProdInsertInput').removeClass('error');
    $('#insertProdModal #categoryProdInsertInput').removeClass('error');
    $('#insertProdModal #priceProdInsertInput').removeClass('error');
    $('#errorSmallId').css("display", "none");
    if (product.id && product.name && product.category && product.unitPrice){
      this.spinnerService.show();
      var response = this.http.post('http://localhost:9998/RestWebApp/rest/productcatalog/insert', product)
      .map(res => res.json())
      .catch((error: any) => {
        this.spinnerService.hide();
        $('#responseModal').modal('show');
        $('#responseModal div.modal-body').html("Error: " + error.status);
        return Observable.throw(new Error(error.status));
      })
      .toPromise();
        response.then(res => {
          this.spinnerService.hide();
          // $('#responseModal').modal('show');
          // $('#responseModal div.modal-body').html(res.status + "<br>" + res.message);
          if (res.status == "FAILURE"){
            $('#insertProdModal #idProdInsertInput').addClass('error');
            $('#errorSmallId').css("display", "inline");
            console.log("ID already exist.");
          } else {
            this.spinnerService.hide();
            $('#insertProdModal').modal('hide');
            console.log("Inserted: " + JSON.stringify(product));
            var timestamp = new Date().getTime();
            this.ngZone.run(() => this.router.navigate(['home/' + timestamp])); 
          }
        });
      } else {
        if (!product.id){
          $('#insertProdModal #idProdInsertInput').addClass('error');
        }
        if (!product.name){
          $('#insertProdModal #nameProdInsertInput').addClass('error');
        }
        if (!product.category){
          $('#insertProdModal #categoryProdInsertInput').addClass('error');
        }
        if (!product.unitPrice){
          $('#insertProdModal #priceProdInsertInput').addClass('error');
        }
      }
  }

  modify(idProdModify, nameProdModify, categoryProdModify, priceProdModify){
    var product = {
      "id": parseInt(idProdModify),
      "name": nameProdModify.trim(),
      "category": categoryProdModify.trim(),
      "unitPrice": parseFloat(priceProdModify) 
    };
    $('#confirmationEditModal #idProdModifyInput').removeClass('error');
    $('#confirmationEditModal #nameProdModifyInput').removeClass('error');
    $('#confirmationEditModal #categoryProdModifyInput').removeClass('error');
    $('#confirmationEditModal #priceProdModifyInput').removeClass('error');
    if (product.id && product.name && product.category && product.unitPrice){
      this.spinnerService.show();
      var response = this.http.put('http://localhost:9998/RestWebApp/rest/productcatalog/update', product)
      .map(res => res.json())
      .catch((error: any) => {
        $('#responseModal').modal('show');
        $('#responseModal div.modal-body').html("Error: " + error.status);
        return Observable.throw(new Error(error.status));
      })
      .toPromise();
      response.then(res => {
        this.spinnerService.hide();
        //$('#confirmationEditModal').modal('show');
        //$('#confirmationEditModal div.modal-body').html(res.status + "<br>" + res.message);
        if (res.status == "FAILURE"){
          $('#idProdModifyInput').addClass('error');
        } else {
          this.spinnerService.hide();
          $('#confirmationEditModal').modal('hide');
          console.log("Updated: " + JSON.stringify(product));
          var timestamp = new Date().getTime();
          this.ngZone.run(() => this.router.navigate(['home/' + timestamp]));  
        }
      });
    } else {
      if (!product.id){
        $('#confirmationEditModal #idProdModifyInput').addClass('error');
      }
      if (!product.name){
        $('#confirmationEditModal #nameProdModifyInput').addClass('error');
      }
      if (!product.category){
        $('#confirmationEditModal #categoryProdModifyInput').addClass('error');
      }
      if (!product.unitPrice){
        $('#confirmationEditModal #priceProdModifyInput').addClass('error');
      }
    }
  }
}
