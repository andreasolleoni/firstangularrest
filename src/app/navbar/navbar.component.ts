import { Component, OnInit } from '@angular/core';
import { SearchByIdService } from '../search-by-id.service';
import { InsertOrModifyService } from '../insert-or-modify.service';
import { ModalsComponent } from '../modals/modals.component';
import * as $ from "jquery";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  constructor(private searchById: SearchByIdService, private modals: ModalsComponent) {
  }

  displayInsertModal(){
    this.modals.confirmInsert();
  }

  ngOnInit() {
  }

}
