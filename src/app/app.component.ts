import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';

import { SearchComponent } from './search/search.component';
import { InsertComponent } from './insert/insert.component';
import { DeleteComponent } from './delete/delete.component';
import { UpdateComponent } from './update/update.component';
import * as bootstrap from "bootstrap";
import { SearchByIdService } from './search-by-id.service';
import { InsertOrModifyService } from './insert-or-modify.service';
import { RouterModule, Routes, Router } from '@angular/router';
import { SmartTableComponent } from './smart-table/smart-table.component';
import * as $ from "jquery";
import * as _ from "underscore";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Client REST';
  idProdSearch;
  idProdInsert
  nameProdInsert;
  categoryProdInsert;
  priceProdInsert;
  idProdModify;
  nameProdModify;
  categoryProdModify;
  priceProdModify;
  idProdDelete;

  
  constructor() {
  }

  ngOnInit() {
  }
}
