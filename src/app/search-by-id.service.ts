import { Injectable, NgZone } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
import { Product } from './product';
import { HttpModule } from '@angular/http';
import { Http, Response } from '@angular/http';
import { Options } from 'selenium-webdriver/safari';
import { Observable } from 'rxjs/Rx';
import { RouterModule, Routes, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as _ from "underscore";

@Injectable()
export class SearchByIdService {

  constructor(private http: Http, private router: Router, private ngZone: NgZone, private spinnerService: Ng4LoadingSpinnerService) {
  }

  printProd(p, i, l){
    //$('#responseModalLabel').val("List of Products");
    $('#responseModal div.modal-body').append(p.name + " " + p.unitPrice + "$ <br>");
  }

  populateTable(){
    console.log("Populating Table...");
    return this.http.get('http://localhost:9998/RestWebApp/rest/productcatalog/search')
        .map(res => res.json())
        .catch((error: any) => {
          $('#responseModal').modal('show');
          $('#responseModal div.modal-body').html("Error: " + error.status);
          return Observable.throw(new Error(error.status));
        });
  }

  searchAllProd(){
    console.log("search all");
    var response = this.http.get('http://localhost:9998/RestWebApp/rest/productcatalog/search')
        .map(res => res.json())
        .catch((error: any) => {
          $('#responseModal').modal('show');
          $('#responseModal div.modal-body').html("Error: " + error.status);
          return Observable.throw(new Error(error.status));
        })
        .toPromise();
        response.then(res => {
          console.log(res);
          $('#responseModal').modal('show');
          if (_.size(res) == 0){
            $('#responseModal div.modal-body').html("No Products Available");
          } else {
            $('#responseModal div.modal-body').html("List of Products: <br>");
            _.each(res, this.printProd);
          }
        });
  }

  search(idProdSearch) {

    if (!idProdSearch){
      console.log("ID non specified");
      $('#idProdSearchInput').addClass('error');
    } else {
      console.log("Search: " + idProdSearch);
      var response = this.http.get('http://localhost:9998/RestWebApp/rest/productcatalog/search/id/' + idProdSearch)
        .map(res => res.json())
        .catch((error: any) => {
          $('#responseModal').modal('show');
          $('#responseModal div.modal-body').html("Error: " + error.status);
          return Observable.throw(new Error(error.status));
        })
        .toPromise();
        response.then(res => {
          console.log(res);
          $('#responseModal').modal('show');
          if (res.id == 0){
            $('#idProdSearchInput').addClass('error');
            $('#responseModal div.modal-body').html(res.name + "<br>" + res.category);
          } else {
            $('#idProdSearchInput').removeClass('error');
            $('#responseModal div.modal-body').html(res.id + "<br>" + res.name + "<br>" + res.category + "<br>" + res.unitPrice);
          }
        });
    }   
  }

  delete(idProdDelete) {

    this.spinnerService.show();

    if (!idProdDelete){
      console.log("ID not specified");
      $('#idProdDeleteInput').addClass('error');
    } else {
      var response = this.http.delete('http://localhost:9998/RestWebApp/rest/productcatalog/delete/id/' + idProdDelete)
      .map(res => res.json())
      .catch((error: any) => {
        this.spinnerService.hide();
        $('#responseModal').modal('show');
        $('#responseModal div.modal-body').html("Error: " + error.status);
        return Observable.throw(new Error(error.status));
      })
      .toPromise();
      response.then(res => {
        this.spinnerService.hide();
        $('#idProdDeleteInput').removeClass('error');
        $('#responseModal').modal('show');
        $('#responseModal div.modal-body').html(res.status + "<br>" + res.message);
        if (res.status == "FAILURE"){
          $('#idProdDeleteInput').addClass('error');
        } else {
          console.log("Deleted: " + idProdDelete);
          var timestamp = new Date().getTime();
          this.ngZone.run(() => this.router.navigate(['home/' + timestamp])); 
        }
      });
    }
  }
}
