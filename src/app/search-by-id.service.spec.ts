import { TestBed, inject } from '@angular/core/testing';

import { SearchByIdService } from './search-by-id.service';

describe('SearchByIdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchByIdService]
    });
  });

  it('should be created', inject([SearchByIdService], (service: SearchByIdService) => {
    expect(service).toBeTruthy();
  }));
});
