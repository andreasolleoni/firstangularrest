import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Product } from '../product';
import { InsertOrModifyService } from '../insert-or-modify.service';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {

  showHide;
  constructor(private insOrMod: InsertOrModifyService) { 
    this.showHide = true;
  }

  ngOnInit() {
  }

}
