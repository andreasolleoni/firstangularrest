import { Component, OnInit, Input } from '@angular/core';
import { SearchByIdService } from '../search-by-id.service'
import { InsertOrModifyService } from '../insert-or-modify.service';
import { Product } from '../product';

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.css']
})
export class ModalsComponent implements OnInit {
  idProdModify;
  dataRow;
  constructor(private searchById: SearchByIdService, private insOrMod: InsertOrModifyService) {
  }

  confirmInsert(){
    $('#insertProdModal #idProdInsertInput').removeClass('error').val("");
    $('#insertProdModal #nameProdInsertInput').removeClass('error').val("");
    $('#insertProdModal #categoryProdInsertInput').removeClass('error').val("");
    $('#insertProdModal #priceProdInsertInput').removeClass('error').val("");
    $('#errorSmallId').css("display", "none");
    $('#insertProdModal').modal('show');
  }

  confirmDelete(data){
    var self = this;
     this.dataRow = data;
    $('#confirmationDeleteModal').modal('show');
    $('#confirmationDeleteModal div.modal-body').html("Si desidera cancellare il record:  " + data.name + "  (ID " + data.id + ")");
    $('#confirmationDeleteModal button.btn-danger').click(function(){ self.searchById.delete(self.dataRow.id) });
  }

  confirmEdit(data){
    let self = this;
    this.idProdModify = data.id;
    $('#confirmationEditModal #idProdModifyInput').removeClass('error');
    $('#confirmationEditModal #nameProdModifyInput').removeClass('error');
    $('#confirmationEditModal #categoryProdModifyInput').removeClass('error');
    $('#confirmationEditModal #priceProdModifyInput').removeClass('error');
    $('#confirmationEditModal #idProdModifyInput').val(data.id);
    $('#confirmationEditModal #nameProdModifyInput').val(data.name);
    $('#confirmationEditModal #categoryProdModifyInput').val(data.category);
    $('#confirmationEditModal #priceProdModifyInput').val(data.unitPrice);
    $('#confirmationEditModal').modal('show'); 
  }

  callServiceDelete(){
  let self = this;
  self.searchById.delete(self.dataRow.id);
  }
  ngOnInit() {
  }

}
