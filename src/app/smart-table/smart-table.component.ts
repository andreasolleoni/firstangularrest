import { Component, OnInit, NgZone} from '@angular/core';
import { Product } from '../product';
import { SearchByIdService } from '../search-by-id.service';
import { InsertOrModifyService } from '../insert-or-modify.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { HttpModule } from '@angular/http';
import { Http, Response } from '@angular/http';
import { LocalDataSource } from 'ng2-smart-table';
import { TableButtonsComponent } from '../table-buttons/table-buttons.component';
import { RouterModule, Routes, Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { routes } from '../app.module';
import * as _ from "underscore";

@Component({
  selector: 'app-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.css']
})
export class SmartTableComponent implements OnInit {

  public source: LocalDataSource;

  settings = {
    actions: false,
    columns: {
      id: {
        title: 'ID',
        width: "10%"
      },
      name: {
        title: 'Name'
      },
      category: {
        title: 'Category'
      },
      unitPrice: {
        title: 'Price',
        width: "15%"
      },
      button: {
        title: '',
        filter: false,
        class: "colonneTabella",
        sort: false,
        width: "15%",
        type: 'custom',
        renderComponent: TableButtonsComponent,
      }
    }
  };

  data = []

  populateList(p, i, l) {
    this.data.push(p);
  }

  constructor(private searchById: SearchByIdService, private router: Router, private ngZone: NgZone, private spinnerService: Ng4LoadingSpinnerService) {
    this.source = new LocalDataSource();
    this.spinnerService.show();
    searchById.populateTable().subscribe(res => {
      if (_.size(res) == 0) {
        this.spinnerService.hide();
        $('#responseModal').modal('show');
        $('#responseModal div.modal-body').html("No Products Available");
      } else {
        _.each(res, this.populateList.bind(this));
        this.source.load(this.data);
        this.spinnerService.hide();
      }
    });
  }

  ngOnInit() {
    if(this.router.url != "/home"){
      console.log("Redirecting...");
      this.ngZone.run(() => this.router.navigate(["home"]));
    }
  }

}
